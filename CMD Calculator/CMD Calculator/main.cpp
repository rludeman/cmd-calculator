#include <boost/program_options.hpp>
#include <boost/optional.hpp>
#include <iostream>

#define INT_T int64_t
#define MIN INT64_MIN
#define MAX INT64_MAX

namespace po = boost::program_options;

// Determine if a - b would result in overflow
bool checkSubOverflow(INT_T a, INT_T b)
{
	if (a > 0 && b < -(MAX - a))
		return true;
	if (a < 0 && b > -(MIN - a))
		return true;

	return false;
}

// Determine if a + b would result in overflow
bool checkAddOverflow(INT_T a, INT_T b)
{
	if (a > 0 && b > MAX - a)
		return true;
	if (a < 0 && b < MIN - a)
		return true;
	
	return false;
}

// Determine if a * b would result in overflow
bool checkMulOverflow(INT_T a, INT_T b)
{
	if (a > 0 && b > 0 && a > MAX / b)
		return true;
	if (a > 0 && b < 0 && b < MIN / a)
		return true;
	if (a < 0 && b > 0 && a < MIN / b)
		return true;
	if (a < 0 && b < 0 && a < MAX / b)
		return true;

	return false;
}

/*
	CMD Calculator
	This program accepts 2 numbers and an arithmatic 
	operator and returns the result. CMD Calculator 
	takes inputs in the following format:
		>>compute a op b
	where 'a' and 'b' are the operands and 'op' is 
	either '+', '-', '/', or '*'.

	Example usage: 
	>>compute 2 + 3
	5
	>>compute 4 - 8
	-4
	>>compute 5 * -3
	-15
	>>compute 18 / 3
	6
*/
int main(int ac, char** av)
{
	// Help message
	std::string msg = 
		" - compute.exe -\n"
		"This program accepts two integers separated by\n"
		"an arithmatic operator and returns the result.\n"
		"Supported operators are +, -, *, and /.\n"
		"Example usage: \n"
		"    >>compute 2 + 3\n"
		"    5\n"
		"    >>compute -3 * 6\n"
		"    -18\n\n"
		"Allowed options";
	po::options_description visible(msg);
	visible.add_options()
		("help", "Display this message")
		;

	// Declare usage options
	// TODO add support for floating point values
	boost::optional<INT_T> a;
	boost::optional<INT_T> b;
	boost::optional<char> op;
	po::options_description hidden("Hidden options");
	hidden.add_options()
		("operand_a", po::value(&a), "First operand in calculation")
		("operator", po::value(&op), "Operator to be used for calculation")
		("operand_b", po::value(&b), "Second operand in calculation")
		;

	// Make options positional
	po::positional_options_description pd;
	pd.add("operand_a", 1).add("operator", 1).add("operand_b", 1);

	// Aggregate all options
	po::options_description cmdline_options;
	cmdline_options.add(visible).add(hidden);

	// Compile options
	// Disables short argument style to allow negative numbers to work
	int disableShort = po::command_line_style::unix_style ^ po::command_line_style::allow_short;
	po::variables_map vm;
	try
	{
		po::store(po::command_line_parser(ac, av).
			options(cmdline_options).positional(pd).style(disableShort).run(), vm);
	}
	catch (po::invalid_option_value e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}
	po::notify(vm);

	// Display help message
	if (vm.count("help"))
	{
		std::cout << visible << "\n";
		return 1;
	}

	// Confirm all options are present
	if (!a)
	{
		std::cout << "Missing first operand.\n";
		return 1;
	}
	if (!op)
	{
		std::cout << "Missing operator.\n";
		return 1;
	}
	if (!b)
	{
		std::cout << "Missing second operand.\n";
		return 1;
	}

	// Perform calculation
	INT_T result;
	switch (*op)
	{
	case '+':
		if (checkAddOverflow(*a, *b))
		{
			std::cout << "Overflow detected" << std::endl;
			return 1;
		}
		result = *a + *b;
		break;

	case '-':
		if (checkSubOverflow(*a, *b))
		{
			std::cout << "Overflow detected" << std::endl;
			return 1;
		}
		result = *a - *b;
		break;

	case '/':
		if (*b == 0)
		{
			std::cout << "Division by 0" << std::endl;
			return 1;
		}
		result = *a / *b;
		break;

	case '*':
		if (checkMulOverflow(*a, *b))
		{
			std::cout << "Overflow detected" << std::endl;
			return 1;
		}
		result = *a * *b;
		break;

	default:
		std::cout << "Undefined operator" << std::endl;
		return 1;
	}

	// Output result
	std::cout << result << std::endl;

	return 0;
}
