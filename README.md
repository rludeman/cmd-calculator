# CMD Calculator

CMD Calculator is a simple command line calculator implemented in C++.  

## Example usage
General usage:  
`>> compute a op b`  
where a and b are integers and op is +, -, *, or /.

Examples:  
`>> compute 1 + 2`  
`3`  
`>> compute -3 * 9`  
`-27`  

## Input Specification
CMD Calculator supports the following operations: addition (+),  
subtraction (-), multiplication (*), and division (/).  Unsupported  
operations will be greeted by an error message.  

CMD Calculator is limited to 64 bit integer values.  Any inputs or  
calculations that would result in overflow will be greeted by an  
error message.  

## Build instructions
You will find my CMakeLists file in the build folder.  If building fails  
for you, you may need to modify the BOOST_ROOT value in line 9 of the  
CMakeLists file.  


## Credits
Author: Ryan Ludeman  
Date: 4/20/2020  
